<?php

if (!$argv[1]) {
    throw new \Exception("Missing argument");
}

$imagick = new Imagick();

$imagick->setColorspace(Imagick::COLORSPACE_RGB);
$imagick->setResolution(1200, 1200);
$imagick->readImage($argv[1]);
// $imagick->transformImageColorspace(Imagick::COLORSPACE_CMYK);

$imagick->writeImage($argv[2]);
