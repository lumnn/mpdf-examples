<?php

if (!$argv[1]) {
    throw new \Exception("Missing argument");
}

$imagickInfo = new Imagick();

$imagickInfo->readImage($argv[1]);
$info = $imagickInfo->identifyImage();
$isCmyk = $info['colorSpace'] === 'CMYK';

$imagick = new Imagick();

if (!$isCmyk) {
    var_dump("Converting to CMYK");
    $imagick->setColorspace(Imagick::COLORSPACE_CMYK);
    // $imagick->transformImageColorspace(Imagick::COLORSPACE_CMYK);
}

$imagick->setResolution(1200, 1200);
$imagick->readImage($argv[1]);

$imagick->writeImage($argv[2]);
