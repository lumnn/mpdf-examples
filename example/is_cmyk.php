<?php

$imagick = new Imagick();

if (!$argv[1]) {
    throw new \Exception("Missing argument");
}

$imagick->readImage($argv[1]);

$info = $imagick->identifyImage();

var_dump([
    "colorSpace" => $info['colorSpace'],
    "width" => $info['geometry']['width'],
    "height" => $info['geometry']['height'],
    "resolution" => $info["resolution"]
]);
