<?php

require 'vendor/autoload.php';

$mpdf = new \Mpdf\Mpdf([
    'PDFX' => true,
    'PDFXauto' => true,
]);

if (!$argv[1]) {
    throw new \Exception("Missing argument");
}

// setting
$baseMargin = 10;
$borderWidth = 1;
$thinBorderWidth = 0.1;
$shift = 0.75;

// import pdf page
$rgbCmykPDF = $mpdf->setSourceFile($argv[1]);
$page1 = $mpdf->importPage($rgbCmykPDF);

$size = $mpdf->getTemplateSize($page1);
$width = round($size['width']);
$height = round($size['height']);

// some random boxes
// $mpdf->writeHtml("<div style='position: absolute; bottom: 10mm; background: #33FF8F; width: 50mm; height: 50mm;'>50mm x 50mm</div>");
$mpdf->writeHtml("<div style='position: absolute; bottom: 9mm; right: 9mm; background: yellow; border: 1mm solid red; width: 43mm; height: 13mm;'>${width} x ${height}</div>");

$mpdf->useTemplate(
    $page1,
    $baseMargin + (($baseMargin + $width) * 0),
    $baseMargin
);
$mpdf->useTemplate(
    $page1,
    $baseMargin + (($baseMargin + $width) * 1),
    $baseMargin
);
$mpdf->useTemplate(
    $page1,
    $baseMargin + (($baseMargin + $width) * 2),
    $baseMargin
);
// first image border on top

$redBorderTop = $baseMargin - $borderWidth;
$redBorderLeft = $baseMargin - $borderWidth;
$dottedBorderTop = $baseMargin;
$dottedBorderLeft = $baseMargin;
$dottedBorderWidth = $width - $borderWidth * 2;
$dottedBorderHeight = $height - $borderWidth * 2;

$mpdf->writeHtml("<div style='position: absolute; top: ${redBorderTop}mm; left: ${redBorderLeft}mm; border: ${borderWidth}mm solid red; width: ${width}mm; height: ${height}mm;'></div>");
$mpdf->writeHtml("<div style='position: absolute; top: ${dottedBorderTop}mm; left: ${dottedBorderLeft}mm; border: ${borderWidth}mm dotted rgba(0, 0, 0, 1); opacity: .25; width: ${dottedBorderWidth}mm; height: ${dottedBorderHeight}mm;'></div>");


$mpdf->useTemplate(
    $page1,
    $baseMargin + (($baseMargin + $width) * 0),
    $baseMargin + (($baseMargin + $height) * 1)
);
$mpdf->useTemplate(
    $page1,
    $baseMargin + (($baseMargin + $width) * 1),
    $baseMargin + (($baseMargin + $height) * 1)
);
$mpdf->useTemplate(
    $page1,
    $baseMargin + (($baseMargin + $width) * 2),
    $baseMargin + (($baseMargin + $height) * 1)
);

// resized
$mpdf->useTemplate(
    $page1,
    $baseMargin + (($baseMargin + $width) * 0),
    $baseMargin + (($baseMargin + $height) * 2),
    30,
    10
);

$thinBoxWidth = $width - ($thinBorderWidth * 2);
$thinBoxHeight = $height - ($thinBorderWidth * 2);

$mpdf->useTemplate(
    $page1,
    $left0 = $baseMargin + (($baseMargin + $width) * 0),
    $top0 = $baseMargin + (($baseMargin + $height) * 3)
);
$mpdf->writeHtml("<div style='font-size: 1mm; text-align: middle; background-clip: padding-box; position: absolute; top: ${top0}mm; left: ${left0}mm; border: ${thinBorderWidth}mm solid black; width: ${thinBoxWidth}mm; height: ${thinBoxHeight}mm;'>Perfectly Middle</div>");

$mpdf->useTemplate(
    $page1,
    $left1 = $baseMargin + (($baseMargin + $width) * 0),
    $top1 = $baseMargin + (($baseMargin + $height) * 4)
);
$top1 -= $shift;
$mpdf->writeHtml("<div style='font-size: 1mm; text-align: middle; backround-clip: padding-box; position: absolute; top: ${top1}mm; left: ${left1}mm; border: ${thinBorderWidth}mm solid black; width: ${thinBoxWidth}mm; height: ${thinBoxHeight}mm;'>Shifted Bottom</div>");

$mpdf->useTemplate(
    $page1,
    $left2 = $baseMargin + (($baseMargin + $width) * 1),
    $top2 = $baseMargin + (($baseMargin + $height) * 4)
);
$left2 -= $shift;
$mpdf->writeHtml("<div style='font-size: 1mm; text-align: middle; backround-clip: padding-box; position: absolute; top: ${top2}mm; left: ${left2}mm; border: ${thinBorderWidth}mm solid black; width: ${thinBoxWidth}mm; height: ${thinBoxHeight}mm;'>Shiffed Right</div>");

$mpdf->useTemplate(
    $page1,
    $left3 = $baseMargin + (($baseMargin + $width) * 0),
    $top3 = $baseMargin + (($baseMargin + $height) * 5)
);
$left3 += $shift;
$mpdf->writeHtml("<div style='font-size: 1mm; text-align: middle; backround-clip: padding-box; position: absolute; top: ${top3}mm; left: ${left3}mm; border: ${thinBorderWidth}mm solid black; width: ${thinBoxWidth}mm; height: ${thinBoxHeight}mm;'>Shifted Left</div>");

$mpdf->useTemplate(
    $page1,
    $left4 = $baseMargin + (($baseMargin + $width) * 1),
    $top4 = $baseMargin + (($baseMargin + $height) * 5)
);
$top4 += $shift;
$mpdf->writeHtml("<div style='font-size: 1mm; text-align: middle; backround-clip: padding-box; position: absolute; top: ${top4}mm; left: ${left4}mm; border: ${thinBorderWidth}mm solid black; width: ${thinBoxWidth}mm; height: ${thinBoxHeight}mm;'>Shifted Top</div>");

$mpdf->output();
