


1. Check CMYK and sizes

```
php example/is_cmyk.php [file]
```

Result shows:
- colour space (CMYK or RGB)
- width
- height
- resolution

This allows me to check:
- whether file is in CMYK colourspace
- correct aspect ratio of the file
- correct minimum size of the file (to avoid blurry effects)

2. Convert to CMYK if neccessary

```
php example/to_cmyk.php [input] [output]
```

This converts whatever is inserted into the CMYK compatible output.

3. Create aligned print pdf for the printing

```
php example/index.php [input_pdf] > output.pdf
```

This aligns PDF in 2 rows 3 columns file.

This also adds a rectangle for layer boundary as well as dotted 1mm bleed mark.

With the calculations added any kind of file can be arranged.

What this can be used for:
- generating PDF proof from the given PDF label
- generating print template

What are the limitations:
- supplied label design must be PDF
- supplied image must be already CMYK
